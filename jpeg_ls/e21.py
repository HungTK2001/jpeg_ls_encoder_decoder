import jpeg_ls
from PIL import Image
import numpy as np


def read(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    with open(fp, 'rb') as fpp:
        img = Image.open(fpp)
        data = np.asarray(img)

    return data


def read_jpeg(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    img = Image.open(fp, mode='r', formats=['JPEG'])

    return img


def write(fname, data_image):
    """
    Write compressed image data to JPEG-LS file.
    """
    data_buffer = jpeg_ls.encode(data_image)

    with open(fname, 'wb') as fo:
        fo.write(data_buffer.tobytes())


def read_jpeg_ls(fname):
    """
    Read image data from JPEG-LS file.
    """
    with open(fname, 'rb') as fo:
        string_buffer = fo.read()

    data_buffer = np.frombuffer(string_buffer, dtype=np.uint8)

    data = np.asarray(data_buffer)
    data_image = jpeg_ls.decode(data)

    return data_image


def encode_to_jpeg_ls(data_image):
    """
    Encode grey-scale image via JPEG-LS using CharLS implementation.
    """

    if data_image.dtype == np.uint16 and np.max(data_image) <= 255:
        data_image = data_image.astype(np.uint8)

    data_buffer = jpeg_ls.encode(data_image)

    return data_buffer


# read jpeg file and convert to png file
file_path = 'test/example_2.jpg'
data = Image.open(file_path)
data = data.convert('P')
data.save('test/test_left_hello.png')

fname_img = 'test/test_left_hello.png'
data_image = read(fname_img)

# Compress image data to a sequence of bytes.
data_buffer = jpeg_ls.encode(data_image)
# Write compressed image data to JPEG-LS file.
with open('file.jls', 'wb') as fo:
    fo.write(data_buffer.tobytes())
