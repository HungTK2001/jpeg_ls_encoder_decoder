import cv2

# Open the .jls file
img = cv2.imread("file.jls")
print(img)

# Save the image as a .jpeg file
cv2.imwrite("output.jpeg", img)
