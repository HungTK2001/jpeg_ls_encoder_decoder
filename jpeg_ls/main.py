import os
import jpeg_ls
from PIL import Image
import numpy as np


# Read image data from file-like object using PIL.  Return Numpy array.
def read(fp):
    with open(fp, 'rb') as fpp:
        img = Image.open(fpp)
        data = np.asarray(img)

    return data


# Read jpeg file using PIL
def read_jpeg(fp):
    img = Image.open(fp, mode='r', formats=['JPEG'])

    return img


# Write data to file
def write(fname, data_image):
    with open(fname, 'wb') as fo:
        fo.write(data_image.tobytes())


# Read image data from JPEG-LS file.
def read_jpeg_ls(fname):
    with open(fname, 'rb') as fo:
        string_buffer = fo.read()

    data_buffer = np.frombuffer(string_buffer, dtype=np.uint8)
    return data_buffer


# Encode grey-scale image via JPEG-LS using CharLS implementation.
def encode_to_jpeg_ls(data_image):
    if data_image.dtype == np.uint16 and np.max(data_image) <= 255:
        data_image = data_image.astype(np.uint8)

    data_buffer = jpeg_ls.encode(data_image)

    return data_buffer


# Encode and decode to JPEG-LS standard
def encode_and_decode():
    jls_fpath = input("Enter the path of the JPEG-LS file: ")
    data_jls_image = read_jpeg_ls(jls_fpath)

    # Sizes.
    size_png = os.path.getsize(jls_fpath)
    print("Size of jls_file_path: ", size_png)

    data_image_b = jpeg_ls.decode(data_jls_image)
    print("Size of the decompression image: ", len(data_image_b.tobytes()))
    print("Ratio of file: ", len(data_image_b.tobytes())/size_png)
    data_encode = jpeg_ls.encode(data_image_b)
    jls_output_fpath = input("Enter the path of the output JPEG-LS file: ")
    write(jls_output_fpath+'.jls', data_encode)
    print('Successfully compression and decompression to JPEG-LS standard')


def encode_jpeg_to_jpeg_ls():
    jpeg_fpath = input("Enter the path of the JPEG file: ")
    size_jpeg = os.path.getsize(jpeg_fpath)
    data = Image.open(jpeg_fpath)
    data = data.convert('P')

    tmp_png_fpath = 'test/tmp_png_file.png'
    data.save(tmp_png_fpath)

    data_image = read(tmp_png_fpath)
    # Compress image data to a sequence of bytes.
    data_buffer = jpeg_ls.encode(data_image)

    jls_output_fpath = input("Enter the path of the output JPEG-LS file: ")
    write(jls_output_fpath+'.jls', data_buffer)
    size_jpeg_ls = os.path.getsize(jls_output_fpath+'.jls')
    print('Ratio of jpeg/jpeg-ls: ', size_jpeg/size_jpeg_ls)
    print('Convert successfully')


def encode_jpeg_ls_to_jpeg():
    jls_fpath = input("Enter the path of the JPEG-LS file: ")
    size_jls = os.path.getsize(jls_fpath)
    data_jls_image = read_jpeg_ls(jls_fpath)

    data_image_b = jpeg_ls.decode(data_jls_image)
    print("Size of the decompression image: ", len(data_image_b.tobytes()))

    img = Image.fromarray(data_image_b)

    jpeg_output_fpath = input("Enter the path of the output JPEG file: ")
    img.save(jpeg_output_fpath+'.jpg')

    size_jpeg = os.path.getsize(jpeg_output_fpath+'.jpg')
    print("Ration jpeg_ls/ jpeg: ", size_jls/size_jpeg)
    print("Convert successfully")


def compress_ratio_comparison():
    png_fpath = input("Enter the path of the PNG file: ")
    data_image = read(png_fpath)

    # Compress image data to a sequence of bytes.
    data_buffer = jpeg_ls.encode(data_image)

    # Sizes.
    size_png = os.path.getsize(png_fpath)

    print('Result of JPEG-LS compression:')
    print('\nSize of uncompressed image data: ', len(data_image.tobytes()))
    print('Size of PNG compression data file:  ', size_png)
    print('Size of JPEG-LS compression data:  ', len(data_buffer))
    print('Ratio of JPEG-LS compression/ PNG compression:  ',
          len(data_buffer) / size_png)


# program for user select the option, read file from path
def main():
    while (True):
        print("\n")
        print("--------------------------------------------------")
        print("JPEG-LS Compression/Cecompression Program")
        print("Select the option")
        print("1. Compression and decompression to JPEG-LS standard")
        print("2. Convert to JPEG-LS standard to JPEG standard")
        print("3. Convert to JPEG standard to JPEG-LS standard")
        print("4. JPEG-LS and PNG compression ratio comparison")
        print("5. Exit")
        choice = int(input("Enter your choice: "))
        if choice == 1:
            encode_and_decode()
        elif choice == 2:
            encode_jpeg_ls_to_jpeg()
        elif choice == 3:
            encode_jpeg_to_jpeg_ls()
        elif choice == 4:
            compress_ratio_comparison()
        elif choice == 5:
            exit()
        else:
            print("Invalid choice")
            main()


main()
