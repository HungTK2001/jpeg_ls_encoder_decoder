import jpeg_ls
from PIL import Image
import numpy as np


def read_jpeg(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    img = Image.open(fp, mode='r', formats=['JPEG'])
    return img


def read_jpeg_ls(fname):
    """
    Read image data from JPEG-LS file.
    """
    with open(fname, 'rb') as fo:
        string_buffer = fo.read()

    data_buffer = np.frombuffer(string_buffer, dtype=np.uint8)

    return data_buffer


# read file jls with read_jpeg_ls function, result: invalid data when encode
jls_fname_img = 'file_example2.jls'
data_jls_image = read_jpeg_ls(jls_fname_img)
data_image_b = jpeg_ls.decode(data_jls_image)
data_encode = jpeg_ls.encode(data_image_b)
with open('test/test_223.jls', 'wb') as fo:
    fo.write(data_encode.tobytes())
