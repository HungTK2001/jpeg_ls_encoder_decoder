

import os

import jpeg_ls
# import PIL.Image
from PIL import Image
import numpy as np
import sys
# np.set_printoptions(threshold=sys.maxsize)


def read(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    with open(fp, 'rb') as fpp:
        img = Image.open(fpp)
        data = np.asarray(img)

    return data


def read_jpeg(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    img = Image.open(fp, mode='r', formats=['JPEG'])
    # data = np.asarray(img)

    return img


def write(fname, data_image):
    """
    Write compressed image data to JPEG-LS file.
    """
    data_buffer = jpeg_ls.encode(data_image)

    with open(fname, 'wb') as fo:
        fo.write(data_buffer.tobytes())


def read_jpeg_ls(fname):
    """
    Read image data from JPEG-LS file.
    """
    with open(fname, 'rb') as fo:
        string_buffer = fo.read()

    data_buffer = np.frombuffer(string_buffer, dtype=np.uint8)

    # data = np.asarray(data_buffer)
    # data_image = jpeg_ls.decode(data)

    # Done.
    return data_buffer
    # return data_image


def encode_to_jpeg_ls(data_image):
    """
    Encode grey-scale image via JPEG-LS using CharLS implementation.
    """

    if data_image.dtype == np.uint16 and np.max(data_image) <= 255:
        data_image = data_image.astype(np.uint8)

    data_buffer = jpeg_ls.encode(data_image)

    return data_buffer


# Read in an image from an existing PNG file.
# fname_img = 'test/gray_raw.png'
# data_image = read(fname_img)
# print(data_image)

# fname_img = 'test/test_left.png'
# data_image = read(fname_img)
# print(data_image)

# # read png file and save to .dat file
# fname_img = 'test/gray_raw.png'
# data_image = read(fname_img)
# write('test/gray_raw.dat', data_image)

# # read .dat file and save to .jls file
# fname_img = 'test/gray_raw.dat'
# data_image = read(fname_img)
# data_image = data_image.squeeze()
# buff = encode_to_jpeg_ls(data_image)
# with open('test/gray_raw.jls', 'wb') as fo:
#     fo.write(buff.tobytes())

# read jpeg file
#file_path = 'test/leftt.jpeg'
# file_path = 'test/example2.jpg'
# data = read_jpeg(file_path)
# data = Image.open(file_path)
# data = data.convert('P')
# print(data)
# data.save('test/test_left_hello.png')
# print(data)


# write('test/gray_raw.jls', data_image)

# read file jls with read function, result: not define file name
# jls_fname_img = 'test/T8C0E0.JLS'
# data_jls_image = read(jls_fname_img)
# print(data_jls_image)

# read file jls with read_jpeg_ls function, result: invalid data when encode
jls_fname_img = 'file_example2.jls'
data_jls_image = read_jpeg_ls(jls_fname_img)
data_image_b = jpeg_ls.decode(data_jls_image)
# data_image = np.asarray(data_image_b)
# print(data_image.dtype)
# data_image_b = data_image_b.transpose(2, 0, 1).reshape(3, -1)

# np.asarray(data_image_b.transpose(2, 0, 1).reshape(3, -1))

# out_array = np.squeeze(data_image_b)
# print(out_array)
data_encode = jpeg_ls.encode(data_image_b)
print(data_encode)

with open('test/test_223.jls', 'wb') as fo:
    fo.write(data_encode.tobytes())

# data_image_b.save('test/test_left_hello.png')
# im = Image.fromarray(data_image_b)


# im = im.convert('')
# print(im)
# im.show()
# im.save('test/test_m3p.png')


#data_buffer = jpeg_ls.encode(data_jls_image)
# data_buffer = encode_to_jpeg_ls(data_jls_image)
# print('Size of JPEG-LS encoded data:  ', len(data_buffer))


# fname_img = 'test/test_left_hello.png'
# data_image = read(fname_img)
# print(data_image)

# # This input image should be a numpy array.
# print('Type: ', data_image.dtype)
# print('Shape: ', data_image.shape)

# # Compress image data to a sequence of bytes.
# data_buffer = jpeg_ls.encode(data_image)
# print(data_buffer)

# # Sizes.
# size_png = os.path.getsize(fname_img)

# print('\nSize of uncompressed image data: ', len(data_image.tobytes()))
# print('Size of PNG encoded data file:  ', size_png)
# print('Size of JPEG-LS encoded data:  ', len(data_buffer))

# with open('file.jls', 'wb') as fo:
#     fo.write(data_buffer.tobytes())
# # Decompress.
# data_image_b = jpeg_ls.decode(data_buffer)
# print(data_image_b)

# # Compare image data, before and after.
# is_same = (data_image == data_image_b).all()
# print('\nRestored data is identical to original?\n', str(is_same))
