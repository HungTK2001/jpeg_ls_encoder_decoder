import os

import jpeg_ls
from PIL import Image
import numpy as np


def read(fp):
    """Read image data from file-like object using PIL.  Return Numpy array.
    """
    with open(fp, 'rb') as fpp:
        img = Image.open(fpp)
        data = np.asarray(img)

    return data


# Read in an image from an existing PNG file.
jls_fpath = input("Enter the path of the image file: ")
data_image = read(jls_fpath)
img = Image.fromarray(data_image)
print("Image", img)
img = img.convert('L')

jls_output_fpath = input("Enter the path of the image file: ")
img.save(jls_output_fpath)

# data_image1 = read('test/car_mode_p.png')
# # Compress image data to a sequence of bytes.
# data_buffer = jpeg_ls.encode(data_image1)

# # Sizes.
# size_png = os.path.getsize('test/car_mode_p.png')

# print('\nSize of uncompressed image data: ', len(data_image1.tobytes()))
# print('Size of PNG encoded data file:  ', size_png)
# print('Size of JPEG-LS encoded data:  ', len(data_buffer))
