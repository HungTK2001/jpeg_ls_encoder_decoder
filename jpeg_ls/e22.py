import jpeg_ls
from PIL import Image
import numpy as np


def read_jpeg_ls(fname):
    """
    Read image data from JPEG-LS file.
    """
    with open(fname, 'rb') as fo:
        string_buffer = fo.read()

    data_buffer = np.frombuffer(string_buffer, dtype=np.uint8)

    return data_buffer


# read file jls with read_jpeg_ls function
jls_fname_img = 'file.jls'
data_jls_image = read_jpeg_ls(jls_fname_img)
data_image_b = jpeg_ls.decode(data_jls_image)

im = Image.fromarray(data_image_b)
# im = im.convert('')
print(im)
im.show()
im.save('test/example_before.jpg')
